﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class healthBarManagement : MonoBehaviour
{
    private float currentHealth;
    private float maxHealth;

    public GameObject healthBarUI;
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("firstSetValues", 1);
    }

    // Update is called once per frame
    void Update()
    {
        //calculo inicial de salud.
        slider.value = CalculateHealth();

        if (gameObject.tag == "EnemyBerserk" || gameObject.tag == "DeadEnemyIgnore")
        {
            if (currentHealth < maxHealth)
            {
                //mostrar la barra de salud en los enemigos solo si hay perdido parte de la salud.
                healthBarUI.SetActive(true);
            }
        }

        //Evitar que las restauraciones de salud del personaje principal aumenten la salud a más del máximo permitido.
        if (currentHealth > maxHealth + 0.1f)
        {
            gameObject.GetComponent<ThirdPersonCharacter>().health = gameObject.GetComponent<ThirdPersonCharacter>().maxHealth;
        }

        if (gameObject.tag == "EnemyBerserk" || gameObject.tag == "DeadEnemyIgnore") 
            healthBarUI.transform.LookAt(Camera.main.transform); //mirar a cámara siempre con la barra informativa.
    }

    //Valor de salud en escala [0,1].
    float CalculateHealth()
    {
        currentHealth = gameObject.GetComponent<ThirdPersonCharacter>().health;
        return (gameObject.GetComponent<ThirdPersonCharacter>().health / gameObject.GetComponent<ThirdPersonCharacter>().maxHealth);
    }

    //Fijación inicial de salud máxima y de salud actual.
    void firstSetValues()
    {
        currentHealth = gameObject.GetComponent<ThirdPersonCharacter>().health;
        maxHealth = gameObject.GetComponent<ThirdPersonCharacter>().maxHealth;
        slider.value = CalculateHealth();
    }
}
