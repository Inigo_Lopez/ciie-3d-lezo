﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class DamageNeedle : MonoBehaviour
{
    public AudioClip SonidoDano;
    public float DamagePerDelta;

    //Mientras nos mantendramos en una trampa durante su colisión, nos causará daño en el tiempo.
    //Caso de trampas con collider tipo trigger.
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "MainPlayer")
        {
            //tomamos el AudioSource del empty object de su hijo, no el AudioSource del MainPlayer root.
            AudioSource audioMainPlayerDano = other.gameObject.transform.Find("SonidoDano").gameObject.GetComponent<AudioSource>();
            if (!audioMainPlayerDano.isPlaying)
            {
                audioMainPlayerDano.clip = SonidoDano;
                audioMainPlayerDano.priority = 200;
                audioMainPlayerDano.loop = false;
                audioMainPlayerDano.volume = 0.2f;
                audioMainPlayerDano.Play();
            }
            other.gameObject.GetComponent<ThirdPersonCharacter>().health -= Time.deltaTime * DamagePerDelta;
        }
    }

    //Mientras nos mantendramos en una trampa durante su colisión, nos causará daño en el tiempo.
    //Caso de trampas con collider con físicas (no trigger).
    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.gameObject.tag == "MainPlayer")
        {
            //tomamos el AudioSource del empty object de su hijo, no el AudioSource del MainPlayer root.
            AudioSource audioMainPlayerDano = collision.collider.gameObject.transform.Find("SonidoDano").gameObject.GetComponent<AudioSource>();
            if (!audioMainPlayerDano.isPlaying)
            {
                audioMainPlayerDano.clip = SonidoDano;
                audioMainPlayerDano.priority = 200;
                audioMainPlayerDano.loop = false;
                audioMainPlayerDano.volume = 0.2f;
                audioMainPlayerDano.Play();
            }
            collision.collider.gameObject.GetComponent<ThirdPersonCharacter>().health -= Time.deltaTime * DamagePerDelta;
        }
    }
}
