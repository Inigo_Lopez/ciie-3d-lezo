﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class gestionarSalud : MonoBehaviour
{
    public float initialGreenLength;
    public GameObject greenBar;
    public float curHealth;
    public float maxHealth;

    public float lastHealth;

    Vector3 greenPos;

    public float timer;
    float time = 2;

    void Awake()
    {

        //loads enemy health value from healthScript
        curHealth = transform.parent.gameObject.GetComponentInParent<ThirdPersonCharacter>().health;
        maxHealth = transform.parent.gameObject.GetComponentInParent<ThirdPersonCharacter>().maxHealth;

        //stores two health values, will come in later
        lastHealth = curHealth;
    }

    /* ANTIGUO SCRIPT PARA GESTION DE SALUD.
     * NO SE REALIZA USO DEL MISMO EN LA ÚLTIMA VERSION.
     */

    void Update()
    {

        timer -= Time.deltaTime;


        if (timer <= 0)
        {
            timer = 0.05f;

            if (lastHealth > curHealth)
            {
                greenPos = greenBar.transform.position;
                greenPos.x = (lastHealth - curHealth) / curHealth;
                greenBar.transform.position = greenPos;
                lastHealth = curHealth;
            }
        }

        curHealth = transform.parent.gameObject.GetComponentInParent<ThirdPersonCharacter>().health;
        maxHealth = transform.parent.gameObject.GetComponentInParent<ThirdPersonCharacter>().maxHealth;

        Vector3 greenScale = greenBar.transform.localScale;
        greenScale = greenScale - new Vector3(0.1f,0,0);
        transform.localScale = greenScale;

    }
}
