﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace camaraMusica
{
    public class GestionarZonas : MonoBehaviour
    {

        /* La idea sería ponerle una colisión de tipo Trigger sobre planos invisibles para activar y
         * para ir cargando la musica por regiones, además de los textos informativos. 
         * De la misma forma se activan los registros para checkpoints.
         */

        private AudioSource audioReference;
        private AudioSource audioReferencePopUp;

        public AudioClip[] trackMusic;

        public GameObject panel1;
        public GameObject panel2;
        public GameObject panel3;
        public GameObject panel4;
        public GameObject panelFinal;
        public GameObject panelLlaves;

        public AudioClip popUpSound;

        private static GameObject deleteReference;

        private CheckPointCheck checkPointChange;
        private GameObject mainPlayer;

        public GameObject TorreVigilancia;
        public GameObject TorreArquera;

        private void Awake()
        {
            audioReference = GetComponent<AudioSource>();
            audioReferencePopUp = GameObject.Find("SonidoPopUp").GetComponent<AudioSource>();
            mainPlayer = GameObject.FindGameObjectWithTag("MainPlayer");
        }

        // Start is called before the first frame update
        void Start()
        {
            audioReference.loop = true;
            audioReference.volume = 0.05f;
            audioReference.priority = 255;
            audioReference.clip = trackMusic[0];
            audioReference.Play();

            if (panel1 != null)
                panel1.SetActive(false);
            if (panel2 != null)
                panel2.SetActive(false);
            if (panel3 != null)
                panel3.SetActive(false);
            if (panel4 != null)
                panel4.SetActive(false);

            checkPointChange = GameObject.FindGameObjectWithTag("CPoint").GetComponent<CheckPointCheck>();
            
            //Check for checkPoint
            if (checkPointChange.passedCheckPoint)
            {
                mainPlayer.transform.position = checkPointChange.lastCheckpointPosition;
            }
        }

        public void gameOver()
        {
            audioReference.loop = false;
            audioReference.volume = 0.3f;
            audioReference.priority = 128;
            audioReference.clip = trackMusic[1];
            audioReference.Play();

        }

        // Update is called once per frame
        void Update()
        {

        }

        /*Gestión de los paneles de control de zonas:
         * 
         *  - Efecto de sonido
         *  - Cambios de música
         *  - Muestra de textos de ayuda
         *  - Registro de checkpoints.
         */ 

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "MusicZone1")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;

                //También es el panel 3 de información
                Destroy(other.gameObject);
                audioReference.loop = true;
                audioReference.volume = 0.05f;
                audioReference.priority = 255;
                audioReference.clip = trackMusic[2];
                audioReference.Play();

                panel3.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel3;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "HelpPanel1")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;

                Destroy(other.gameObject);

                panel1.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel1;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "HelpPanel2")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;

                Destroy(other.gameObject);

                panel2.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel2;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "HelpPanel4")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;

                Destroy(other.gameObject);

                panel4.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel4;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "ChangeTowers")
            {
                //Desactivamos torre de vigilancia (por eficiencia y consumo)
                //Activamos la torre arquera.

                //Solo mantenemos una de las instancias de la Torre en cada momento, hasta este panel solo estaba la de vigilancia
                //A partir de la fase final, solo se encuentra activa la torre de arquero.

                Destroy(other.gameObject);

                TorreVigilancia.SetActive(false);
                TorreArquera.SetActive(true);

            }

            if (other.gameObject.name == "FinalGame")
            {
                //Gestión del final del juego en el poblado.

                Destroy(other.gameObject);

                audioReference.loop = true;
                audioReference.volume = 0.05f;
                audioReference.priority = 255;
                audioReference.clip = trackMusic[3];
                audioReference.Play();

                panelFinal.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panelFinal;
                Invoke("destroyText", 10f);
                Invoke("loadMenuAgain", 11f);

                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "PanelKeys")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;

                Destroy(other.gameObject);

                panelLlaves.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panelLlaves;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }
        }

        private void destroyText()
        {
            deleteReference.SetActive(false);
        }

        private void loadMenuAgain()
        {
            SceneManager.LoadScene(0); //Carga del menu principal nuevamente.
        }
    }
}