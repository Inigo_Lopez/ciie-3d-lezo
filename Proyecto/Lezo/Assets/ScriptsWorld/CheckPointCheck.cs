﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointCheck : MonoBehaviour
{
    //Instancia del patrón singleton.
    private static CheckPointCheck instance;

    //Ultima posición conocida de los checkpoints.
    public Vector3 lastCheckpointPosition;
    public bool passedCheckPoint = false;

    private void Awake()
    {
        //Mantenemos un único objeto que referencie a dicho componente script.
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        } else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
