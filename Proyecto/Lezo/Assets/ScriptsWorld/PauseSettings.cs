﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseSettings : MenusFunctions
{
    public static bool PauseActive = false;
    public GameObject pauseMenu;

    // Update is called once per frame

    //Gestión del menú de pausa una vez este se pulsa.
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (PauseActive)
                ResumeGame();
            else
                PauseGame();
        }
    }

    //Ocultación de la pausa y continúa el juego.
    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        PauseActive = false;
    }

    //Pausa del juego y se para el juego.
    private void PauseGame()
    {
        PauseActive = true;
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
    }

    public void LoadMenu()
    {
        PauseActive = false;
        Time.timeScale = 1f;
        LoadSceneByIndex(0);        // 0 is Menu Index
    }
}
