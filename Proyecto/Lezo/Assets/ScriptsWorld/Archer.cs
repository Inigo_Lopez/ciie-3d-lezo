﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class Archer : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject target;
    private GameObject target_m;
    private GameObject vigilante;
    private GameObject visor_v;
    private int counts;

    //==========================================================================================
    float time = 0.0f;
    float angulo_global_x = 0.0f;
    float angulo_parcial_x = 0.0f;
    float angulo_global_y = 0.0f;
    float angulo_parcial_y = 0.0f;
    public bool objetivo_detectado = false;
    private bool en_vigilancia = true;
    Vector3 targetDir;
    float angle;

    Collider m_collider;
    public float speed;
    //==========================================================================================

    void Start()
    {
        counts = 0;
        target = GameObject.FindGameObjectWithTag("MainPlayer"); //ThirdPerson controller
        vigilante = transform.parent.gameObject;
        visor_v = GameObject.FindGameObjectWithTag("VisionVigilante");
        m_collider = visor_v.GetComponent<Collider>();
        speed = 2.5f;
    }

    private void Update()
    {

    }

    /* Algoritmo para la torre de arquero.
     *  Si colisiona su "haz" de vista con el personaje y estaba en vigilancia (activo),
     *  se fija al protagonista como objetivo en un margen de 700 ticks del fixed update.
     */ 

    private void OnTriggerEnter(Collider other)
    {
        if (en_vigilancia)
        {
            if (other.gameObject.tag == "MainPlayer")
            {

                objetivo_detectado = true;
                counts = 700;


            }
        }

    }

    /* Si se sale de su vista, vuelve a estar en vigilancia a la espera de otra colisión
     */
     
    private void OnTriggerExit(Collider other)
    {
        en_vigilancia = true;

    }

    
    void FixedUpdate()
    {

        /* Si estaba detectado, consume tiempo de fijación y se ajusta a la posición del personaje
         * para dispararle. Decrementa contador para consumir su tiempo activo. Se deshabilita el collider
         * principal de la torre para evitar interacciones de los proyectiles con ella.
         */

        //==========================================================================================
        if (objetivo_detectado)
        {
            vigilante.transform.LookAt(target.transform);
            vigilante.transform.Rotate(2, 0, 0, Space.Self);

            m_collider.enabled = false;
            decreaseCounts();
        }
        else
        {
            /* Si no lo ha encontrado efectúa búsquedas realizando ajustes de ángulos en un rango pseudoaleatorio
             * cercano a la posición real del personaje principal.
             * Estos ajustes se ejectúan cada cierto intervalo de tiempo controlado por una velocidad de fijación/rastreo
             */

            time += Time.deltaTime;

            if (time > speed)
            {
                targetDir = target.transform.position - vigilante.transform.position;
                angle = -Vector3.SignedAngle(targetDir, vigilante.transform.forward, Vector3.up);

                if (vigilante.transform.rotation.eulerAngles.x - 180 < 0)
                    angulo_global_x = Random.Range(-15, -10);
                else
                    angulo_global_x = Random.Range(10, 45);

                angulo_global_y = Random.Range(angle - 10, angle + 10);

                time = 0.0f;
            }

            angulo_parcial_x += (angulo_global_x / (speed / Time.deltaTime));
            angulo_parcial_y += (angulo_global_y / (speed / Time.deltaTime));

            vigilante.transform.localRotation = Quaternion.Euler(angulo_parcial_x, angulo_parcial_y, 0);
        }

        //==========================================================================================

    }

    /* Procedimiento para consumir tiempo de fijación una vez ha detectado al protagonista 
     */

    void decreaseCounts()
    {
        counts--;

        if (counts <= 0)
        {
            counts = 0;
            objetivo_detectado = false;
            en_vigilancia = false;
            m_collider.enabled = true;
        }
    }
}
