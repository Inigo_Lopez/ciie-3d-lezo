﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class FollowController : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject personaje;

    void Start()
    {
        personaje = gameObject.transform.Find("ThirdPersonController").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = personaje.transform.position;
    }
}
